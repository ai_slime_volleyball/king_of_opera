from sys import exit

import pygame
import pygame.font
from pygame.locals import *

from Event.base_event import BaseEvent

import Config.event_config as EVENT_CON

opera = pygame.image.load("screen/icon/opera.png")
firefox = pygame.image.load("screen/icon/firefox.png")
chrome = pygame.image.load("screen/icon/chrome.png")
safari = pygame.image.load("screen/icon/safari.png")
IE = pygame.image.load("screen/icon/IE.png")
w,h=opera.get_size()
s=0.23
w=int(w*s)
h=int(h*s)
opera = pygame.transform.smoothscale(opera,(w,h))
firefox = pygame.transform.smoothscale(firefox,(w,h))
chrome = pygame.transform.smoothscale(chrome,(w,h))
IE = pygame.transform.smoothscale(IE,(w,h))
w,h=safari.get_size()
s=0.23
w=int(w*s)
h=int(h*s)
safari = pygame.transform.smoothscale(safari,(w,h))

background = pygame.image.load("screen/bg-light.jpg")

browser_alpha = int(255*0.85)
opera.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
firefox.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
chrome.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
safari.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
IE.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)

double = pygame.image.load("screen/skill/double.png")
double.fill((255, 255, 255, int(255*0.8)), None, pygame.BLEND_RGBA_MULT)
w,h=double.get_size()
doubleb=pygame.transform.scale(double,(int(0.9*w),int(0.9*h)))

pygame.font.init()
teamfont = pygame.font.Font('screen/FONT01.TTF', 40)
nclr = (97,66,125)
num = []
fname = "screen/num/0-1.png"
name_alpha = int (255*0.8)
for j in range(2) :
    fname=fname[0:13]+str(j+1)+fname[14:];
    for i in range(10) :
        fname=fname[0:11]+str(i)+fname[12:];
        tmp=pygame.image.load(fname)
        w,h=tmp.get_size()
        s = 0.363
        w=int(w*s)
        h=int(h*s)
        tmp=pygame.transform.smoothscale(tmp,(w,h))
        tmp.fill((255, 255, 255, name_alpha), None, pygame.BLEND_RGBA_MULT)
        num.append(tmp)

class _sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.image=m
        self.rect=self.image.get_rect()
        self.rect.topleft=(x,y)

class double_sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.time = 0
        self.image=m
        self.rect=self.image.get_rect()
        self.rect.topleft=(x,y)
    def change(self) :
        self.time+=1
        if self.time == 50 :
            self.time = 0
        if self.time<25 :
            self.image=doubleb
        else :
            self.image=double

class num_sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,n) :
        pygame.sprite.Sprite.__init__(self)
        self.image=num[n]
        self.rect=self.image.get_rect()
        self.rect.topleft=(x,y)
        self.time = 0
    def change_num(self,n) :
        self.time+=1
        if self.time == 50 :
            self.time = 0
        if self.time<25 :
            self.image=num[n]
        else :
            self.image=num[n+10]
    def init_time(self) :
        self.time = 25
            

all_group = pygame.sprite.Group()
#browser_group = pygame.sprite.Group()
#name_group = pygame.sprite.Group()
#num_group = pygame.sprite.Group()
num0_sprites = []
num1_sprites = []
num2_sprites = []
num3_sprites = []
for i in range(5) :
    num0_sprites.append(num_sprite(613+37*i,56,0))
    num1_sprites.append(num_sprite(613+37*i,56+95,0))
    num2_sprites.append(num_sprite(613+37*i,56+95*2,0))
    num3_sprites.append(num_sprite(613+37*i,56+95*3,0))
#    num_group.add(num0_sprites[i])
#    num_group.add(num1_sprites[i])
#    num_group.add(num2_sprites[i])
#    num_group.add(num3_sprites[i])
    all_group.add(num0_sprites[i])
    all_group.add(num1_sprites[i])
    all_group.add(num2_sprites[i])
    all_group.add(num3_sprites[i])
num0_sprites[1].init_time()
num1_sprites[1].init_time()
num2_sprites[1].init_time()
num3_sprites[1].init_time()
num0_sprites[3].init_time()
num1_sprites[3].init_time()
num2_sprites[3].init_time()
num3_sprites[3].init_time()

double_sprites = []
for i in range(4) :
    double_sprites.append(double_sprite(796,66+95*i,double))

class EventInitScoreboard(BaseEvent) :
    
    def __init__(self,env,ms):
        self.env = env
        self.priority = ms
        self.name = []
        self.name.append(teamfont.render(self.env["player"][0].name, 1, nclr))
        self.name.append(teamfont.render(self.env["player"][1].name, 1, nclr))
        self.name.append(teamfont.render(self.env["player"][2].name, 1, nclr))
        self.name.append(teamfont.render(self.env["player"][3].name, 1, nclr))
        for i in range(4) :
            tmp = _sprite(613,20+95*i,self.name[i])
            #name_group.add(tmp)
            all_group.add(tmp)

        self.browser = []
        ttt = 0
        for i in range(4) :        
            if self.env["player"][i].browser=="ie" :
                self.browser.append(IE)
            elif self.env["player"][i].browser=="safari" :
                self.browser.append(safari)
                ttt = 5
            elif self.env["player"][i].browser=="chrome" :
                self.browser.append(chrome)
            elif self.env["player"][i].browser=="firefox" :
                self.browser.append(firefox)
            elif self.env["player"][i].browser=="opera" :
                self.browser.append(opera)
        for i in range(4) :
            tmp = _sprite(792,17+95*i - ttt,self.browser[i])
            #browser_group.add(tmp)
            all_group.add(tmp)
        
    def do_action(self) :
        self.env["uic"].add_event(EventDrawScoreboard(self.env,self.priority+EVENT_CON.TICKS_PER_TURN))
class EventDrawScoreboard(BaseEvent) :
    def __init__(self,env,ms) :
        self.env = env
        self.screen = self.env["screen"]
        self.score = []
        self.factor = []
        for i in range(4) :
            self.score.append(self.env["player"][i].score)
            self.factor.append(self.env["player"][i].score_factor)
        for i in range(4) :
            if self.factor[i] != 1 :
                all_group.add(double_sprites[i])
                double_sprites[i].change()
            else :
                tmp_group = pygame.sprite.Group()
                tmp_group.add(double_sprites[i])
                tmp_group.clear( self.screen,background)
                tmp_group.remove(double_sprites[i])
                all_group.remove(double_sprites[i])
        self.priority=ms
    def change_score(self,score,sprites) :
        dig = 10000
        for i in range(5) :
            t = score//dig
            score%=dig
            dig//=10
            sprites[i].change_num(t)

    def do_action(self) :
        self.change_score(self.score[0],num0_sprites)
        self.change_score(self.score[1],num1_sprites)
        self.change_score(self.score[2],num2_sprites)
        self.change_score(self.score[3],num3_sprites)
        #all_group.clear( self.screen,background)
        #all_group.draw( self.screen)
        #pygame.display.update()
        self.env["uic"].add_event(EventDrawScoreboard(self.env,self.priority+EVENT_CON.TICKS_PER_TURN))

