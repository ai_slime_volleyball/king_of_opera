import pygame
from Event.base_event import BaseEvent
import Config.event_config as EVENT_CON

class EventPyEvent(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env
    
    def do_action(self):
        for e in pygame.event.get():
            if e.type == pygame.locals.QUIT:
                self.env["pyQUIT"] = True
            elif e.type == pygame.locals.KEYDOWN:
                if e.key == pygame.locals.K_1:
                    #print("press!")
                    self.env["py1"] = True
                if e.key == pygame.locals.K_2:
                    #print("press!")
                    self.env["py2"] = True
                if e.key == pygame.locals.K_3:
                    #print("press!")
                    self.env["py3"] = True
                if e.key == pygame.locals.K_4:
                    #print("press!")
                    self.env["py4"] = True
                if e.key == pygame.locals.K_ESCAPE:
                    #print("press ESC")
                    self.env["pyESC"] = True
                if e.key == pygame.locals.K_a:
                    #print("YO.OY")
                    self.env["pyA"] = True
                if e.key == pygame.locals.K_o:
                    self.env['gamec'].pause()
                if e.key == pygame.locals.K_p:
                    self.env['gamec'].resume()


            elif e.type == pygame.locals.KEYUP:
                if e.key == pygame.locals.K_1:
                    #print("unpress")
                    self.env["py1"] = False
                if e.key == pygame.locals.K_2:
                    #print("unpress")
                    self.env["py2"] = False
                if e.key == pygame.locals.K_3:
                    #print("unpress")
                    self.env["py3"] = False
                if e.key == pygame.locals.K_4:
                    #print("unpress")
                    self.env["py4"] = False
                if e.key == pygame.locals.K_ESCAPE:
                    #print("unpress ESC")
                    self.env["pyESC"] = False
                if e.key == pygame.locals.K_a:
                    self.env["pyA"] = False

        self.env["uic"].add_event(EventPyEvent(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
