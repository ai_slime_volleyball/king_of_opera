import math
import random
from AI.base_ai import BaseAi
from AI.ai_config import *
from AI.Q_learning import Q_learning
import Config.game_config as GAME_CON

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "Random"
        self.target = "center"
        self.target_pos = (0, 0)
        self.players = None

    def get_action(self, players):
        #1->撞A, 2->撞B, 3->撞C, 0->回中間
        choice = [0]
        i = 1
        for player in players:
            choice.append(i)
            i += 1
        return random.choice(choice)  

    def decide(self, helper):        
        if self.target == None:
            self.players = helper.get_others_distance()
            action = self.get_action(helper.get_others_distance())
            
            if action == 3 and len(self.players) == 3:
                self.target = self.players[2]
                self.target_pos = self.players[2].get_position()
            elif action >= 2 and len(self.players) >= 2:
                self.target = self.players[1]
                self.target_pos = self.players[1].get_position()
            elif action >= 1 and len(self.players) >= 1:
                self.target = self.players[0]
                self.target_pos = self.players[0].get_position()
            else:
                self.target = "center"
                self.target_pos = (0, 0)

        if helper.me.check_facing(self.target_pos, 0.1):
            #player.cal_vector(pos): 計算 pos 相對於player的位置 ( 回傳(r, theta) )
            r, theta = helper.me.cal_vector(self.target_pos)
            if r < 50:
                self.target = None
                return ActionUnPress
            else:
                return ActionPress
        else:
            return ActionUnPress
