import math
from AI.base_ai import BaseAi
from AI.ai_config import *

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "找人撞"

    def decide(self, helper):
        if helper.get_others_degree() == []:
                return ActionNull
        other = helper.get_others_degree()[0] # 最快會轉到的那隻
        if helper.me.check_facing(other.get_position()):
            return ActionPress
        else:
            return ActionUnPress
