import math
from Tools.vector import Vector

from Event.base_event import BaseEvent
from Items.item_base import BaseItem 

import Config.event_config as EVENT_CON
import Config.env_config as ENV_CON
            

class MoveEvent(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
    
    def do_action(self):
        for player in self.env['live_player']:
            self.env["gamec"].add_event(EventMovePlayer(player.char, self.priority+1))
        for ball in self.env['balls']:
            self.env["gamec"].add_event(EventMoveBall(ball, self.priority+1))
        self.env["gamec"].add_event(EventItemCollision(
            [player.char for player in self.env['live_player']] + self.env['balls'], self.env['walls'],
            self.priority+2))
        
        self.env["gamec"].add_event(MoveEvent(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventMovePlayer(BaseEvent):
    def __init__(self, player, priority):
        self.player = player
        self.priority = priority

    def do_action(self):
        delta = (self.priority - self.player.update_timestamp)/1000
        if self.player.lock > 0 or self.player.pressed == True:
            if self.player.lock > 0:
                self.player.lock -= delta
                #if abs(math.cos(self.player.direction - self.player.degree) * self.player.speed) < ENV_CON.TERMINATE_SPEED:
                #    self.player.lock = -1
                if self.player.lock < 0:
                    self.player.direction = self.player.degree
                    self.player.last_colli = None
                a = -self.player.speed * ENV_CON.COLLISION_DRAG / self.player.mass
            else:
                a = self.player.acc - self.player.speed * ENV_CON.START_DRAG / self.player.mass
                self.player.degree = self.player.direction
            
            if abs(a) < 1e-6:
                a = 0
            self.player.speed += a * delta
        else:
            self.player.speed = 0
            self.player.direction += self.player.rotate_dir * delta * math.pi * ENV_CON.PLAYER_OMEGA
            self.player.degree = self.player.direction
            while self.player.direction >= 2 * math.pi:
                self.player.direction -= 2 * math.pi
            while self.player.direction < 0:
                self.player.direction += 2 * math.pi
        self.player.move(delta)
        self.player.update_timestamp = self.priority


class EventItemCollision(BaseEvent):
    def __init__(self, items, walls, priority):
        self.items = items
        self.walls = walls
        self.priority = priority

    def do_action(self):
        for i in range(len(self.items)):
            p1 = self.items[i]
            # item and item
            for j in range(i+1, len(self.items)):
                p2 = self.items[j]
                d = Vector.dis(p1.get_center(), p2.get_center())
                if d <= p1.radius + p2.radius:
                    BaseItem.collision(p1, p2, d)
            # item and wall
            d = Vector.dis(p1.get_center(), ENV_CON.ROOM_CTR)
            if d >= ENV_CON.ROOM_RADIUS:
                theta = Vector(p1.get_center()[0] - ENV_CON.ROOM_CTR[0], p1.get_center()[1] - ENV_CON.ROOM_CTR[1]).get_degree()
                for wall in self.walls:
                    if wall.theta1 <= theta <= wall.theta2:
                        r1 = Vector(math.cos(p1.direction), math.sin(p1.direction))
                        n = Vector((ENV_CON.ROOM_CTR[0] - p1.position['x']) / d,
                                (ENV_CON.ROOM_CTR[1] - p1.position['y']) / d)
                        l = Vector((p1.position['y'] - ENV_CON.ROOM_CTR[1]) / d,
                                (ENV_CON.ROOM_CTR[0] - p1.position['x']) / d) 
                        v1 = Vector(p1.speed * r1.dot(n), p1.speed * r1.dot(l))
                        v1p = Vector(-v1.x, v1.y)
                        p1.speed = math.sqrt(v1p.x**2 + v1p.y**2)
                        dtheta = math.acos(n.x)
                        if n.y < 0:
                            dtheta *= -1
                        
                        dr = d - ENV_CON.ROOM_RADIUS + 1
                        dx = dr * math.cos(dtheta)
                        dy = dr * math.sin(dtheta)
                        p1.position['x'] += dx
                        p1.position['y'] += dy
                        
                        if v1p.x == 0:
                            if abs(v1p.y) < 1e-6: pass
                            elif v1p.y > 0: p1.direction = math.pi / 2 + dtheta
                            else: p1.direction = -math.pi / 2 + dtheta
                        else: p1.direction = math.atan(v1p.y / v1p.x) + dtheta
                        if v1p.x < 0: p1.direction += math.pi
                        while p1.direction < 0:
                             p1.direction += 2 * math.pi
                        if p1.speed != 0:
                            p1.lock = max(-ENV_CON.COLLISION_DRAG / p1.mass * math.log(ENV_CON.TERMINATE_SPEED / p1.speed), 0.5)
                        else:
                            p1.lock = 0
                

class EventMoveBall(BaseEvent):
    def __init__(self, ball, priority):
        self.ball = ball
        self.priority = priority

    def do_action(self):
        self.ball.move((self.priority - self.ball.update_timestamp)/1000)
        self.ball.update_timestamp = self.priority


class EventBachi(BaseEvent):
    def __init__(self, lufi, items, priority):
        self.lufi = lufi
        self.items = items
        self.priority = priority

    def do_action(self):
        for item in self.items:
            if self.lufi != item:
                d = Vector.dis(self.lufi.get_center(), item.get_center())
                item.direction = Vector(item.get_center()[0] - self.lufi.get_center()[0],
                        item.get_center()[1] - self.lufi.get_center()[1]).get_theta()
                item.speed = ENV_CON.BACHI/(d**2)
                item.lock = ENV_CON.BACHI_LOCK
                item.last_colli = self.lufi
