import random
import pygame
from Tools.vector import Vector
import Config.env_config as ENV_CON
import math

class BaseItem:
    def __init__(self, dic):
        self.speed = dic["speed"]
        self.position = dic["position"] #{'x': 30, 'y': 30}
        self.radius = dic["radius"]
        self.degree = dic["degree"]
        self.direction = self.degree
        self.mass = dic["mass"]
        self.update_timestamp = 0
        self.last_colli = None

    def get_center(self):
        return (self.position['x'], self.position['y'])

    def move(self, t):
        self.position['x'] += (self.speed * math.cos(self.direction)) * t
        self.position['y'] += (self.speed * math.sin(self.direction)) * t

    @staticmethod
    def collision(item1, item2, d):
        r1 = Vector(math.cos(item1.direction), math.sin(item1.direction))
        r2 = Vector(math.cos(item2.direction), math.sin(item2.direction))
        n = Vector((item2.position['x'] - item1.position['x']) / d,
                (item2.position['y'] - item1.position['y']) / d)
        l = Vector((item1.position['y'] - item2.position['y']) / d,
                (item2.position['x'] - item1.position['x']) / d)

        v1 = Vector(item1.speed * r1.dot(n), item1.speed * r1.dot(l))
        v2 = Vector(item2.speed * r2.dot(n), item2.speed * r2.dot(l))
        m1, m2 = item1.mass, item2.mass
        v1p = Vector((v1.x * (m1 - m2) + v2.x * m2 * 2) / (m1 + m2), v1.y)
        v2p = Vector((v2.x * (m2 - m1) + v1.x * m1 * 2) / (m1 + m2), v2.y)
        item1.speed = math.sqrt(v1p.x * v1p.x + v1p.y * v1p.y)
        item2.speed = math.sqrt(v2p.x * v2p.x + v2p.y * v2p.y)
        dtheta = math.acos(n.x)
        if n.y < 0:
            dtheta *= -1
        
        dr = (item1.radius + item2.radius - d) / 2 + 1
        dx = dr * math.cos(dtheta)
        dy = dr * math.sin(dtheta)
        
        if v1p.x == 0:
            if abs(v1p.y) < 1e-6: pass
            elif v1p.y > 0: item1.direction = math.pi / 2 + dtheta
            else: item1.direction = -math.pi / 2 + dtheta
        else: item1.direction = math.atan(v1p.y / v1p.x) + dtheta
        if v1p.x < 0: item1.direction += math.pi
        while item1.direction < 0:
             item1.direction += 2 * math.pi
        if v2p.x == 0:
            if abs(v2p.y) < 1e-6: pass
            elif v2p.y > 0: item2.direction = math.pi / 2 + dtheta
            else: item2.direction = -math.pi / 2 + dtheta
        else: item2.direction = math.atan(v2p.y / v2p.x) + dtheta
        if v2p.x < 0: item2.direction += math.pi
        while item2.direction < 0:
            item2.direction += 2 * math.pi
        if item1.speed != 0:
            item1.lock = max(-ENV_CON.COLLISION_DRAG / item1.mass * math.log(ENV_CON.TERMINATE_SPEED / item1.speed), 0.5)
        else:
            item1.lock = 0
        if item2.speed != 0:
            item2.lock = max(-ENV_CON.COLLISION_DRAG / item2.mass * math.log(ENV_CON.TERMINATE_SPEED / item2.speed), 0.5)
        else:
            item2.lock = 0
        item1.position['x'] -= dx
        item1.position['y'] -= dy
        item2.position['x'] += dx
        item2.position['y'] += dy
        item1.last_colli = item2
        item2.last_colli = item1

        power = min(1, (item1.speed+item2.speed)/2/ENV_CON.C)
        sound = pygame.mixer.Sound('sounds/hit2.wav')
        sound.set_volume(power)
        sound.play()
        if power == 1:
            sound = pygame.mixer.Sound('sounds/hit.wav')
            sound.play()

