import math
from AI.base_ai import BaseAi
from AI.ai_config import *

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "輕按反轉"
        self.target = None
        self.click = False

    def decide(self, helper):
        if self.click:
            self.click = False
            return ActionUnPress
        
        if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
            self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻

        if helper.me.check_facing(self.target.get_position()):
            return ActionPress
        else:
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                self.click = True # 輕按一下等一下馬上放掉
                return ActionPress
            return ActionUnPress
