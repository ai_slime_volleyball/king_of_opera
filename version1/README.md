2014 台大椰林資訊營 python AI Challenge 小遊戲

# 系統需求
* python3
* pygame

# 使用方法
`python3 main.py [NUM [NUM [NUM [NUM]]]]`  
輸入 AI 編號即可以指定 AI 操控角色，training為產生Q_table，Q為Q-learning結果，1為default AI，留空則以左上數字 1~4 手動控制

# 狀態
對手清單,依照相對於自己的距離由近到遠排序(3)
對手清單,依照相對於自己延目前方向旋轉的角度由小到大排序(3)
對手清單,依照分數由高到低排序(3)
player是否已死亡(2^4)
player是否處於暈眩(><)狀態(2^4)
player座標位置(間距50，81^4)
player的速度(間距50，約5^4)
player現在眼睛面對的方向(間距30度，12^4)
player現在旋轉的方向(2^4)

# 目前state數
[我離圓心的距離, 我speed＋我角度 or 不轉, 對手離圓心的距離, 對手speed＋我角度 or 不轉, 對手><, 我和對手相對角度, 我和對手分數相對高低]
= [5 * (8 * 2 + 1)]^2 * 2 * 8 * 2
= 231200 (* 2 = 50萬)

# usage
helper.me.get_Qstate(helper.get_others_degree()[0]) 回傳字串，代表state
player.Qscore記錄將對手撞出次數
game_config.py中TOTAL_TIME可更改遊戲時間
game_config.py中TIMEUP為True即代表遊戲時間到（但有delay，分數及action能有幾秒會增加、更動），可使用GAME_CON.TIMEUP取值
event_config.py中FPS可調速度

# info
ROOM_RADIUS = 225
ROOM_CTR = ( 310, 270 )
speed 0~400（或以上，不確定上限）
degree 0~2pi
speed起跳 7.2