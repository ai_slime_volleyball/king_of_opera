from Items.character import Character

import Config.env_config as ENV_CON

class Player():
    def __init__(self):
        self.Qscore = 0

        self.tick = 0

        self.score = 0 
        self.score_factor = 1
        self.score_double_tick = 0

        self.invisible = False
        self.invisible_tick = 0

        self.browser_tick = 0

        self.name = "jizz"
        self.ai = None
        self.char = None #(character)
        self.browser = "opera"
        self.origin_browser = None
        self.org_browser = None

        self.live_timestamp = 0
        self.live_tmp = 0
    def add_score(self, deltascore):
        #if deltascore > 0 :
        self.score += int(self.score_factor*deltascore)
        self.score = max(0, self.score)
        #else:
        #    self.score += self.score_factor

    def live_tick(self, time):
        if self.live_timestamp >= 0:
            delta = time - self.live_timestamp
            self.live_tmp += (delta)
            self.live_timestamp = time

        self.score += (self.live_tmp // 99)
        self.live_tmp %= 99

    def died(self, time):
        delta = time - self.live_timestamp
        self.live_tmp += (delta)
        self.live_timestamp = -1

        self.score += (self.live_tmp // 99)
        self.live_tmp %= 99

    def alive(self, time):
        self.live_timestamp = time

    def magic_tick(self, tick):
        delta = tick - self.tick
        self.tick = tick
        self.score_double_tick = max(self.score_double_tick - delta, 0)
        self.invisible_tick = max(self.invisible_tick - delta, 0)
        self.browser_tick = max(self.browser_tick - delta, 0)

        if self.score_double_tick > 0:
            self.score_factor = 1.6
        else:
            self.score_factor = 1

        self.invisible = (self.invisible_tick > 0)

        if self.browser_tick > 0 :
            self.browser = "ie"
            self.char.acc = ENV_CON.PLAYER_IE_ACC
        else :
            if self.org_browser != None :
                self.browser = self.org_browser
                self.org_browser = None
            if self.char.acc_tick == 0:
                self.char.acc = ENV_CON.PLAYER_NORMAL_ACC

    def magic_browser(self, tick):
        if self.org_browser == None:
            self.org_browser = self.browser

        self.browser_tick += tick

    def magic_score(self, tick):
        self.score_double_tick += tick

    def magic_invisible(self, tick):
        self.invisible_tick += tick

    def replay(self, timestamp):
        self.char = Character(dict(
            speed = 0,
            position = {'x': 0, 'y': 0 },
            degree = 0,
            radius = ENV_CON.PLAYER_SIZES[1] / 2,
            mass = ENV_CON.PLAYER_MASS,
        ))
        self.char.update_timestamp = timestamp
