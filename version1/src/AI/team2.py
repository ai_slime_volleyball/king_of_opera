'''
    AI template
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *
import random

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "Minimax"
        self.legalActions = [ActionNull, ActionUnPress, ActionPress]

    def decide(self, helper):
        AlphaBetaAgent()
        return random.choice(self.legalActions)

class AlphaBetaAgent():
    def __init__(self):
        self.legalActions = [ActionNull, ActionUnPress, ActionPress]

    def getAction(self, gameState):
        def max_value(gameState, depth, num_ghost, alpha, beta):
            if gameState.isWin() or gameState.isLose() or depth == 0:
                return self.evaluationFunction(gameState)
            v = -float("inf")
            for action in gameState.getLegalActions(0):
                v = max(v, min_value(gameState.generateSuccessor(0, action), depth, 1, num_ghost, alpha, beta))
                if v > beta: return v
                alpha = max(alpha, v)
            return v

        def min_value(gameState, depth, agent_index, num_ghost, alpha, beta):
            if gameState.isWin() or gameState.isLose() or depth == 0:
                return self.evaluationFunction(gameState)
            v = float("inf")
            if agent_index == num_ghost:
                for action in gameState.getLegalActions(agent_index):
                    v = min(v, max_value(gameState.generateSuccessor(agent_index, action), depth - 1, num_ghost, alpha, beta))
                if v < alpha: return v
                beta = min(beta, v)
                return v
            else:
                for action in gameState.getLegalActions(agent_index):
                    v = min(v, min_value(gameState.generateSuccessor(agent_index, action), depth, agent_index + 1, num_ghost, alpha, beta))
                if v < alpha: return v
                beta = min(beta, v)
                return v
            alpha = -float("inf")
            beta = float("inf")
            for action in gameState.getLegalActions(0):
                v = min_value(gameState.generateSuccessor(0, action), self.depth, 1, gameState.getNumAgents() - 1, alpha, beta) 
               if v > alpha: alpha = v
               best_action = action
            return best_action

