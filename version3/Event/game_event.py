﻿import imp
import math
import random
import sys
import traceback
import pygame

from Event.base_event import BaseEvent
import Event.physics_event
import Event.ui_event
import Event.io_event
import Event.scoreboard_event
import Event.timer_event
import Event.skill_event
import Event.clear_event
import Event.end_event
import Event.skill_animation_event

from player import Player
from Items.character import Character
from Items.wall import Wall 
from Items.ball import Ball
import Items.cards
#from Items.card import Card

import Config.event_config as EVENT_CON
import Config.game_config as GAME_CON
import Config.env_config as ENV_CON
import AI.ai_config as AI_CON
from helper import *

import exception

class EventBeforePause:
    def __init__(self, bgm):
        self.bgm = bgm

    def do_action(self):
        self.bgm.pause()


class EventAfterPause:
    def __init__(self, bgm):
        self.bgm = bgm

    def do_action(self):
        self.bgm.unpause()


class EventHelper:
    inst = None
    def __init__(self, env):
        EventHelper.inst = self
        self.env = env

    def find_any_point(self):
        import random
        theta = random.random()*2*math.pi
        r = random.randint(0, ENV_CON.ROOM_RADIUS)
        x = r*math.cos(theta) + ENV_CON.ROOM_CTR[0]
        y = r*math.sin(theta) + ENV_CON.ROOM_CTR[1]
        return x, y

    def find_safe_point_for_card(self):
        import random
        found = False
        x = 0
        y = 0
        avgr = (40 + 40)/4
        # RANDOM ALGOR
        while found == False:
            found = True
            theta = random.random()*2*math.pi
            r = random.randint(0, ENV_CON.ROOM_RADIUS - ENV_CON.BALL_SIZE)
            x = r*math.cos(theta) + ENV_CON.ROOM_CTR[0]
            y = r*math.sin(theta) + ENV_CON.ROOM_CTR[1]
            for other_player in self.env["live_player"] :
                ox, oy = other_player.char.get_center()
                dist2 = (x - ox)**2 + (y - oy)**2
                if (dist2 < (other_player.char.radius + avgr)**2 ):
                    found = False
                    break
        return (x, y)

    def find_safe_point_for_player(self, player):
        import random
        found = False
        x = 0
        y = 0
        # RANDOM ALGOR
        while found == False:
            found = True
            theta = random.random()*2*math.pi
            r = random.randint(0, ENV_CON.ROOM_RADIUS//2)
            x = r*math.cos(theta) + ENV_CON.ROOM_CTR[0]
            y = r*math.sin(theta) + ENV_CON.ROOM_CTR[1]
            for other_player in self.env["live_player"] :
                ox, oy = other_player.char.get_center()
                dist2 = (x - ox)**2 + (y - oy)**2
                if (dist2 < (other_player.char.radius + player.char.radius)**2 ):
                    found = False
                    break

            for ball in self.env["balls"] :
                ox, oy = ball.get_center()
                dist2 = (x - ox)**2 + (y - oy)**2
                if (dist2 < (ball.radius + player.char.radius)**2 ):
                    found = False
                    break

        return (x, y)

    def get_dist_char_card(self, char, card):
        import math
        dist = math.sqrt((char.position['x'] - card.position['x'])**2 +
                         (char.position['y'] - card.position['y'])**2)
        return dist - char.radius - 20


class EventStartGame(BaseEvent):
    def __init__(self, env, ailist, rnd_seed, priority):
        self.priority = priority
        self.ailist = ailist
        self.env = env
        self.random_seed = rnd_seed

    def do_action(self):
        random.seed(self.random_seed)
        self.env['bgm'] = pygame.mixer.Channel(0)
        self.env['bgm'].play(pygame.mixer.Sound('sounds/bgm.wav'))
        self.env['gamec'].set_pause_event(EventBeforePause(self.env['bgm']), EventAfterPause(self.env['bgm']))

        self.env["timer"] = 0
        # setup charater
        browsers = ["opera", "firefox", "chrome", "safari"]
        skillnames = ["Swap", "ScoreDouble", "Invisible", "Power", "ChaseBeat", "Abs", "Cold"]
        self.env["player"] = []
        self.env["live_player"] = []
        self.env["died_player"] = []
        for i in range(4):
            p1 = Player()
            try:
                loadtmp = imp.load_source('', './AI/team' + str(self.ailist[i]) + '.py')
                print("Load team" + str(self.ailist[i]) + ".py")
                p1.ai_index = self.ailist[i]
            except IndexError:
                loadtmp = imp.load_source('', './Auto/click' + str(i + 1) + '.py')
                p1.ai_index = -1
            except Exception as err:
                loadtmp = imp.load_source('', './Auto/click' + str(i + 1) + '.py')
                p1.ai_index = -2
                traceback.print_exc()
                #typ, msg, trac = sys.exc_info()
                #print(msg)
                #traceback.print_tb(trac, file=sys.stdout)
                print('-'*40)
            p1.score = GAME_CON.INIT_SCORE
            p1.ai = loadtmp.TeamAI(self.env)
            p1.index = i
            try:
                tttmp = p1.ai.name
            except:
                print(str(i) + " AI has no name.")
                p1.ai.name = "NULL"
            # READ SKILL_DATA ----------------------
            try:
                p1.SKILL_DATA = loadtmp.SKILL # should be dictionary
            except:
                p1.SKILL_DATA = dict(
                    Swap = 0,
                    ScoreDouble = 0,
                    Invisible = 0,
                    Power = 0,
                    ChaseBeat = 0,
                    Abs = 0,
                    Cold = 0
                )

            for skillname in skillnames:
                try:
                    tmp = loadtmp.SKILL[skillname]
                    #print("find", skillname)
                except:
                    p1.SKILL_DATA[skillname] = 0
            #----------------------------------------
            p1.name = p1.ai.name
            import math
            x = ENV_CON.ROOM_CTR[0] + (ENV_CON.ROOM_RADIUS*6/10)*math.cos(i*2*math.pi/4)
            y = ENV_CON.ROOM_CTR[1] + (ENV_CON.ROOM_RADIUS*6/10)*math.sin(i*2*math.pi/4)

            p1.char = Character(dict(
                speed = 0,
                position = dict(x = x, y = y),
                degree = i*2*math.pi/4,
                radius = ENV_CON.PLAYER_SIZES[1] / 2,
                mass = ENV_CON.PLAYER_MASS,
            ))
            p1.char.rotate_dir = 1
            p1.browser = browsers[i]
            p1.origin_browser = browsers[i]
            self.env["live_player"].append(p1)
            self.env["player"].append(p1) # read from config

        # setup wall
        self.env["const_walls"] = []
        self.env["walls"] = []
        for i in range(12):
            import math
            ni = ((i - 3)%12 + 12)%12
            dic = {}
            dic["theta1"] = ni*(math.pi/6)
            dic["theta2"] = (ni + 1)*(math.pi/6)
            dic["index"] = i
            wall = Wall(dic)
            self.env["const_walls"].append(Wall(dic))

        self.env["pyQUIT"] = False
        self.env["py1"] = False
        self.env["py2"] = False
        self.env["py3"] = False
        self.env["py4"] = False
        self.env['pyESC'] = False
        self.env['pyA'] = False
        self.env["balls"] = []
        self.env["cards"] = []

        self.env["gamec"].add_event(EventDecide(self.env, 400))
        self.env["gamec"].add_event(EventCheckPlayerOutside(self.env, 400))
        self.env["gamec"].add_event(EventCheckBallOutside(self.env, 400))
        self.env["gamec"].add_event(EventTimer(self.env, 400))
        # self.env["gamec"].add_event(EventDebugMsg(self.env, 400))
        self.env["gamec"].add_event(EventMagicTick(self.env, 400))

        self.env["gamec"].add_event(EventPutCard(self.env, 400))
        self.env["gamec"].add_event(EventPutBall(self.env, 400+ GAME_CON.BALL_ADD_TIME))
        self.env["gamec"].add_event(EventPutWall(self.env, 400))

        self.env["gamec"].add_event(EventJudgeCard(self.env, 400))

        self.env["gamec"].add_event(Event.physics_event.MoveEvent(self.env, 400))
        self.env["uic"].add_event(Event.io_event.EventPyEvent(self.env, self.priority))
        self.env["uic"].add_event(Event.clear_event.EventInitClear(self.env, 1))
        self.env["uic"].add_event(Event.ui_event.EventInitGround(self.env, 2))
        self.env["uic"].add_event(Event.ui_event.EventCloseWindow(self.env, self.priority))
        self.env["uic"].add_event(Event.timer_event.EventInitTimer(self.env, 3))
        self.env["uic"].add_event(Event.scoreboard_event.EventInitScoreboard(self.env, 4))
        # Initial of helper
        EventHelper(self.env)
        

class EventCheckPlayerOutside(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        for player in self.env['live_player']:
            x, y = player.char.get_center()
            if (x - ENV_CON.ROOM_CTR[0])**2 + (y - ENV_CON.ROOM_CTR[1])**2  > ENV_CON.ROOM_RADIUS**2 :
                self.env["gamec"].add_event(EventRemovePlayer(self.env, player, self.priority + EVENT_CON.TICKS_PER_TURN))
                self.env["live_player"].remove(player)
                self.env["died_player"].append(player)
        self.env["gamec"].add_event(EventCheckPlayerOutside(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventAddScore(BaseEvent):
    def __init__(self, env, player, addscore, ms, first=True):
        self.env = env
        self.player = player
        self.addscore = addscore
        self.priority = ms
        self.first = first

    def do_action(self):
        if self.addscore>0:
            if self.first:
                sound = pygame.mixer.Sound('sounds/add_score.wav')
                sound.play()
                if self.addscore == 100:
                    self.player.Qscore += 100

            if self.addscore > 13:
                self.player.add_score(13)
                self.env["gamec"].add_event(EventAddScore(self.env, self.player,  
                    self.addscore - 13, self.priority + EVENT_CON.TICKS_PER_TURN, False))
            else:
                self.player.add_score(self.addscore)
        else:
            if -self.addscore > 13:
                self.player.add_score(-13)
                self.env["gamec"].add_event(EventAddScore(self.env, self.player,  
                    self.addscore + 13, self.priority + EVENT_CON.TICKS_PER_TURN, False))
            else:
                self.player.add_score(self.addscore)
                self.player.Qscore -= 500
        

class EventTimer(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms

    def do_action(self):
        if(self.env["timer"] == GAME_CON.TOTAL_TIME):
            self.env["gamec"].add_event(EventGameOver(self.env, self.priority))
        else:
            self.env["timer"] += 1
            self.env["gamec"].add_event(EventTimer(self.env, self.priority + 1000))


class EventDecide(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
        
    def do_action(self):
        for player in self.env["player"]:
            try:
                helper = Helper(player,self.env)
                paras = player.ai.decide(helper)
                skillani = Event.skill_animation_event.EventSkill(self.env, 0)
                if paras == None:
                    raise exception.AIReturnValueError(player.ai.name, "沒有回傳值")

                if paras == AI_CON.ActionNull:
                    pass

                elif paras == AI_CON.ActionPress:
                    player.char.pressed = True

                elif paras == AI_CON.ActionUnPress:
                    if player.char.pressed:
                        player.char.rotate_dir = player.char.rotate_dir*(-1)
                    player.char.pressed = False
                elif paras == AI_CON.ActionSkillScoreDouble:
                    if player.SKILL_DATA["ScoreDouble"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "加倍奉還")

                    player.SKILL_DATA["ScoreDouble"] -= 1
                    skillani.do_action(player.origin_browser, 6)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillScoreDouble(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillInvisible:
                    if player.SKILL_DATA["Invisible"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "你看不見我")

                    player.SKILL_DATA["Invisible"] -= 1
                    skillani.do_action(player.origin_browser, 0)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillInvisible(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillPower:
                    if player.SKILL_DATA["Power"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "霸王色霸氣")

                    player.SKILL_DATA["Power"] -= 1
                    skillani.do_action(player.origin_browser, 2)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillPower(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillAbs:
                    if player.SKILL_DATA["Abs"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "進擊的巨人")

                    player.SKILL_DATA["Abs"] -= 1
                    skillani.do_action(player.origin_browser, 3)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillAbs(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillCold:
                    if player.SKILL_DATA["Cold"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "你聽過lag嗎？")

                    player.SKILL_DATA["Cold"] -= 1
                    skillani.do_action(player.origin_browser, 5)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillCold(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillChaseBeat:
                    if player.SKILL_DATA["ChaseBeat"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "一大堆香蕉")

                    player.SKILL_DATA["ChaseBeat"] -= 1
                    #print("[Skill] Chase Beat by ", player.name)
                    skillani.do_action(player.origin_browser, 4)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillChaseBeat(self.env, player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
                elif paras == AI_CON.ActionSkillSwap:
                    raise exception.AIReturnValueError(player.ai.name, "必須要回傳交換的對象")

                elif paras[0] == AI_CON.ActionSkillWall:
                    wall_list = paras[1]
                    self.env["gamec"].add_event(Event.skill_event.EventSkillAddWall(self.env, wall_list, self.priority + EVENT_CON.TICKS_PER_TURN))

                elif paras[0] == AI_CON.ActionSkillSwap:
                    if player.SKILL_DATA["Swap"] <= 0 :
                        raise exception.SkillExcessError(player.ai.name, "來 你來 來")

                    if len(paras) == 1:
                        raise exception.AIReturnValueError(player.ai.name, "必須要回傳交換的對象")

                    try:
                        toward_player = paras[1].player
                    except:
                        raise exception.AIReturnValueError(player.ai.name, "回傳交換對象錯誤")

                    player.SKILL_DATA["Swap"] -= 1
                    skillani.do_action(player.origin_browser, 1)
                    self.env["gamec"].add_event(Event.skill_event.EventSkillSwap(self.env, player, toward_player, self.priority + GAME_CON.SKILL_OFFSET + EVENT_CON.TICKS_PER_TURN))
            except exception.AIReturnValueError as e:
                print(e.name + " -> " + "AI回傳值錯誤：" + str(e))
            except exception.SkillExcessError as e:
                print(e.name + " -> " + str(e))
            except:
                typ, msg, trac = sys.exc_info()
                print(player.name)
                print(msg)
                traceback.print_tb(trac, file=sys.stdout)
                print('-'*40)
        self.env["gamec"].add_event(EventDecide(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventGameOver(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms

    def do_action(self):
        self.env['bgm'].stop()
        sound = pygame.mixer.Sound('sounds/game_over.wav')
        sound.play()
        self.env["gamec"].stop()
        self.env["uic"].add_event(Event.end_event.EventEnd(self.env, self.priority))


class EventAddPlayer(BaseEvent):
    def __init__(self, env, player, ms):
        self.env = env
        self.player = player
        self.priority = ms

    def do_action(self):
        self.player.replay(self.priority)
        x, y = EventHelper.inst.find_safe_point_for_player(self.player)
        self.player.char.position['x'] = x
        self.player.char.position['y'] = y
        self.player.alive(self.priority)
        self.player.magic_invisible(2)
        self.env["live_player"].append(self.player)
        self.env["died_player"].remove(self.player)


class EventRemovePlayer(BaseEvent):
    def __init__(self, env, player, ms):
        self.env = env
        self.player = player
        # magic, avoid magic things
        self.last_colli = self.player.char.last_colli
        self.priority = ms

    def do_action(self):
        if self.last_colli not in [p.char for p in self.env['player']]:
            ball_trigger_player = None
            try:
                ball_trigger_player = self.last_colli.trigger_player
            except:
                pass

            if ball_trigger_player == None :
                for player in self.env["player"]:
                    if player is self.player : continue
                    self.env["gamec"].add_event(EventAddScore(self.env, player,
                        GAME_CON.SCORE_ADD//3 , self.priority + EVENT_CON.TICKS_PER_TURN))
            else:
                self.env["gamec"].add_event(EventAddScore(self.env, ball_trigger_player,
                    GAME_CON.SCORE_ADD , self.priority + EVENT_CON.TICKS_PER_TURN))
        else:
            toaddplayer = None
            for player in self.env["player"]:
                if player.char is self.last_colli:
                    toaddplayer = player
            self.env["gamec"].add_event(EventAddScore(self.env, toaddplayer,
                GAME_CON.SCORE_ADD , self.priority + EVENT_CON.TICKS_PER_TURN))
        self.player.died(self.priority)
        self.env["gamec"].add_event(EventAddScore(self.env, self.player,
            -GAME_CON.SCORE_ADD , self.priority + EVENT_CON.TICKS_PER_TURN))

        self.env["gamec"].add_event(EventAddPlayer(self.env, self.player, 
            self.priority + EVENT_CON.TICKS_PER_TURN
                          + GAME_CON.REPLAY_DELAY_TIME))


class EventDebugMsg(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        if False :
            # Test of wall
            Found = False
            for wall in self.env["walls"]:
                if (wall.index == (self.env["timer"] % 12)):
                    Found = True
                    break
            
            if Found == False and len(self.env['walls']) < 3:
                ww = self.env["const_walls"][self.env["timer"] % 12]
                self.env["gamec"].add_event(EventAddWall(self.env, ww, 
                    self.priority + EVENT_CON.TICKS_PER_TURN))

        print("walls:")
        for wall in self.env["walls"]:
            print(wall.index)
        print("-"*40)
        print("balls:")
        for ball in self.env["balls"]:
            print(ball)
        print("-"*40)
        for player in self.env["player"]:
            print("player", player.index, "-->")
            print("   mass = ", player.char.mass)
            print("   smile = ", player.char.is_magic_smile(), player.char.magic_smile_timestamp)
            print("   radius = ", player.char.radius)
            print("   lock = ", player.char.lock)
            print("   acc = ", player.char.acc)
        self.env["gamec"].add_event(EventDebugMsg(self.env, self.priority + EVENT_CON.TICKS_PER_TURN
                                                                             + GAME_CON.DEBUG_DELAY_TIME))


class EventMagicTick(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms

    def do_action(self):
        for player in self.env["player"]:
            player.char.magic_tick(self.env["timer"])
            player.magic_tick(self.env['timer'])

        for live_player in self.env["live_player"]:
            live_player.live_tick(self.priority)

        self.env["gamec"].add_event(EventMagicTick(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventJudgeCard(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        #TODO : if there is two player?
        for card in self.env["cards"]:
            for player in self.env["live_player"]:
                if EventHelper.inst.get_dist_char_card(player.char, card) < 0:
                    self.env["cards"].remove(card)
                    self.env["gamec"].add_event(EventTriggerCard(self.env, player, card, self.priority + EVENT_CON.TICKS_PER_TURN))
                    player.char.magic()
                    #print("eat!")
                    sound = pygame.mixer.Sound('sounds/card.wav')
                    sound.play()
                    break

        self.env["gamec"].add_event(EventJudgeCard(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventPutCard(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms

    def do_action(self):
        if random.randint(0, 99) < GAME_CON.CARD_ADD_POSSIBLE+100 :
            x, y = EventHelper.inst.find_safe_point_for_card()
            cc = None
            dise = random.randint(0, 10)
            choosetype = random.randint(0, 100)%2 + 1
            choosecard = None
            if choosetype == 1:
                choosecard = random.choice(
                    (Items.cards.MassMinusingCard,
                    Items.cards.MassAddingCard,
                    Items.cards.SpeedMinusingCard,
                    Items.cards.SpeedAddingCard,
                    Items.cards.ScoreCard)
                )
            else:
                choosecard = random.choice(
                    (Items.cards.MassAddingCard,
                    Items.cards.SpeedAddingCard,
                    Items.cards.ScoreCard)
                )

            cc = choosecard(dict(
                position = dict(x = x, y = y),
                env = self.env,
                r_x = 22.5,
                r_y = 20,
            ), choosetype)

            if cc != None :
                self.env["gamec"].add_event(EventAddCard(self.env, cc, self.priority + EVENT_CON.TICKS_PER_TURN))

        self.env["gamec"].add_event(EventPutCard(self.env, self.priority + EVENT_CON.TICKS_PER_TURN
                                                                         + GAME_CON.CARD_ADD_TIME))


class EventTriggerCard(BaseEvent):
    def __init__(self, env, player, card, ms):
        self.priority = ms
        self.env = env
        self.player = player
        self.card = card

    def do_action(self):
        self.card.trigger(self.player)


class EventAddCard(BaseEvent):
    def __init__(self, env, card, ms):
        self.priority = ms
        self.env = env
        self.card = card

    def do_action(self):
        self.env["cards"] = []
        #self.env["gamec"].add_event(EventRemoveCard(self.env, self.card, self.priority + EVENT_CON.TICKS_PER_TURN
        #                                                                               + GAME_CON.CARD_DELAY_TIME))


class EventRemoveCard(BaseEvent):
    def __init__(self, env, card, ms):
        self.priority = ms
        self.env = env
        self.card = card

    def do_action(self):
        if (self.card in self.env["cards"]):
            self.env["cards"].remove(self.card)


class EventPutBall(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        # TODO : put some constant to config
        # TODO : put to helper
        import random
        found = False
        x = 0
        y = 0
        theta = 0
        # RANDOM ALGOR
        while found == False:
            found = True
            theta = random.random()*2*math.pi
            r = ENV_CON.ROOM_RADIUS-1
            x = r*math.cos(theta) + ENV_CON.ROOM_CTR[0]
            y = r*math.sin(theta) + ENV_CON.ROOM_CTR[1]
            for other_player in self.env["live_player"] :
                ox, oy = other_player.char.get_center()
                dist2 = (x - ox)**2 + (y - oy)**2
                if (dist2 < (other_player.char.radius + 1)**2 ):
                    found = False
                    break

        bb = Ball(dict(
            speed = ENV_CON.BALL_SPEED,
            position = dict(x = x, y = y) ,
            radius = ENV_CON.BALL_SIZE / 2,
            degree = math.pi + theta,
            mass = ENV_CON.BALL_MASS,
            timestamp = self.priority,
        ))
        self.env["gamec"].add_event(EventAddBall(self.env, bb, self.priority + EVENT_CON.TICKS_PER_TURN))
        self.env["gamec"].add_event(EventPutBall(self.env, self.priority + EVENT_CON.TICKS_PER_TURN
                                                                         + GAME_CON.BALL_ADD_TIME))


class EventCheckBallOutside(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        for ball in self.env["balls"]:
            x, y = ball.get_center()
            if (x - ENV_CON.ROOM_CTR[0])**2 + (y - ENV_CON.ROOM_CTR[1])**2  > ENV_CON.ROOM_RADIUS**2 :
                self.env["balls"].remove(ball)
                # Strange, it do as same as EventRemoveBall
        self.env["gamec"].add_event(EventCheckBallOutside(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventAddBall(BaseEvent):
    def __init__(self, env, ball, ms):
        self.env = env
        self.ball = ball
        self.priority = ms

    def do_action(self):
        #sound = pygame.mixer.Sound('sounds/ball_in.wav')
        #sound.play()
        self.env["balls"] = []


class EventRemoveBall(BaseEvent):
    def __init__(self, env, ball, ms):
        self.env = env
        self.ball = ball
        self.priority = ms

    def do_action(self):
        self.env["balls"].remove(self.ball)


class EventPutWall(BaseEvent):
    def __init__(self, env, ms):
        self.priority = ms
        self.env = env

    def do_action(self):
        import random
        Found = False
        tseed = (random.randint(0, 84)) %12
        for wall in self.env["walls"]:
            if (wall.index == (tseed)):
                Found = True
                break

        if Found == False and len(self.env['walls']) < 3:
            ww = self.env["const_walls"][tseed]
            self.env["gamec"].add_event(EventAddWall(self.env, ww,
                                                    self.priority + EVENT_CON.TICKS_PER_TURN))

        self.env["gamec"].add_event(EventPutWall(self.env, self.priority + EVENT_CON.TICKS_PER_TURN
                                                           + GAME_CON.DEBUG_DELAY_TIME))


class EventAddWall(BaseEvent):
    def __init__(self, env, wall, ms):
        self.env = env
        self.wall = wall
        self.priority = ms

    def do_action(self):
        if( self.wall in self.env["walls"]) : return
        self.env["walls"] = []
        self.env["gamec"].add_event(EventRemoveWall(self.env, self.wall, 
            self.priority + EVENT_CON.TICKS_PER_TURN 
                          + GAME_CON.WALL_DELAY_TIME))


class EventRemoveWall(BaseEvent):
    def __init__(self, env, wall, ms):
        self.env = env
        self.wall = wall
        self.priority = ms

    def do_action(self):
        if( self.wall in self.env["walls"]):
            self.env["walls"].remove(self.wall)
