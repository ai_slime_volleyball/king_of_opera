import Event.game_event
import math
import copy
from operator import attrgetter
import Config.env_config as ENV_CON
import Config.game_config as GAME_CON

def cal_arc(x,y):
    arc = math.atan2(y, x)
    if arc < 0:
        arc += 2*math.pi
    return arc

class PlayerHelper():
    def __init__(self, player, live_player, me):
        self.player = player
        self.live_player = live_player
        self.me = me

    def is_lock(self):
        return (self.player.char.lock > 0)

    def is_invisible(self):
        return self.player.invisible

    def is_dead(self):
        return (self.player not in self.live_player) or (not self.me and self.player.invisible)

    def get_position(self):
        x = self.player.char.position['x']-ENV_CON.ROOM_CTR[0]
        y = self.player.char.position['y']-ENV_CON.ROOM_CTR[1]
        return (x, y)

    def get_position_r(self):
        x, y = self.get_position()
        r = math.sqrt(x**2+y**2)
        theta = cal_arc(x,y)
        return (r, theta)

    def get_speed(self):
        return (self.player.char.speed, self.player.char.direction)

    def get_degree(self): #eyes' direction
        return self.player.char.degree

    def get_rotate_dir(self): #clockwise or counterclock
        return self.player.char.rotate_dir

    def get_size(self):
        return self.player.char.radius

    def get_mass(self):
        return self.player.char.mass

    def get_score(self):
        return self.player.score

    def get_skill_count(self, name):
        if self.player.SKILL_DATA[name]:
            return self.player.SKILL_DATA[name]
        else:
            return 0

    def cal_vector(self, pos):
        ox, oy = self.get_position()
        x = pos[0] - ox
        y = pos[1] - oy
        r = math.sqrt(x**2+y**2)
        theta = cal_arc(x,y)
        return (r, theta)

    def cal_rel_degree(self, pos):
        origin = self.get_degree()
        counter = self.get_rotate_dir() == -1
        degree = self.cal_vector(pos)[1]
        arc = 0
        if not counter:
            arc = degree - origin
        else:
            arc = origin - degree
        while arc > 2*math.pi:
            arc -= 2*math.pi
        while arc < 0:
            arc += 2*math.pi
        return arc

    def check_facing(self, pos, deviation=0.05):
        degree = self.cal_rel_degree(pos)
        degree = min(degree, 2*math.pi-degree)
        return degree <= deviation

    #added method
    def get_Qstate(self, helper):
        def dist(x, y, a, b, c):
            return abs(a*x+b*y+c)/math.sqrt(a*a+b*b)
        def is_in_rec(x, y, xa, ya, xb, yb, w):
            d1 = w/2
            d2 = math.sqrt((xb-xa)*(xb-xa)+(yb-ya)*(yb-ya))/2
            if dist(x, y, yb-ya, xa-xb, (ya-yb)*xa+(xb-xa)*ya) <= d1 and dist(x, y, xb-xa, yb-ya, (xa*xa-xb*xb+ya*ya-yb*yb)/2) <= d2:
                return True
            return False

        #死掉或暈倒
        if self.is_lock() or self.is_dead():
            return 18
        r, theta = helper.me.get_position_r()

        #判斷自己離圓心遠近
        if r < 170:
            a = 0
        else:
            a = 1

        #判斷對手中誰和我中間沒人
        flag = False
        for other in helper.others:
            if other.is_dead() or other.is_lock():
                continue
            flag = True
        if flag == False:
            b = 4#全死
        else:
            x0, y0 = self.get_position()
            candidate = []
            for can_target in helper.others:
                if can_target.is_lock() or can_target.is_dead():
                    continue
                tar_x, tar_y = can_target.get_position()
                flag = False
                for other in helper.others:
                    if flag:
                        break
                    if other == can_target or other.is_dead():
                        continue
                    x, y = other.get_position()
                    if is_in_rec(x, y, x0, y0, tar_x, tar_y, 120):
                        flag = True
                if not flag:
                    candidate.append(can_target)
            if len(candidate) > 0:
                target = 0
                max_r = 0
                for can_target in candidate:
                    r, theta = can_target.get_position_r()
                    if r > max_r and r > 170:
                        max_r = r
                        target = can_target
                if target == 0:
                    b = 3
                else:
                    b = helper.others.index(target)
            else:
                b = 3
        if b == 3:
            if helper.others[0].cal_vector(self.get_position())[0] > 200:
                c = 0
            else:
                c = 1
        elif b == 4:
            c = 0
        else:
            if target.cal_vector(self.get_position())[0] > 200:
                c = 0
            else:
                c = 1
        #print(target)
        return a * 9 + b * 2 + c
    def get_Qscore(self):
        return self.player.Qscore
                            
class Helper():
    def __init__(self, player, env):
        self.me = PlayerHelper(player, env['live_player'], True)
        self.others = [PlayerHelper(_other, env['live_player'], False) for _other in env['live_player'] if _other is not player and not _other.invisible]
        self.env = env

    def get_field_radius(self):
        return ENV_CON.ROOM_RADIUS

    def get_walls(self):
        return map(lambda x: (x.theta1, x.theta2), self.env['walls'])

    def get_remain_time(self):
        return GAME_CON.TOTAL_TIME - self.env['timer']

    def get_cards(self):
        return [(_card.position['x']-ENV_CON.ROOM_CTR[0], _card.position['y']-ENV_CON.ROOM_CTR[1]) for _card in self.env['cards']]

    def get_chance(self):
        return [(_card.position['x']-ENV_CON.ROOM_CTR[0], _card.position['y']-ENV_CON.ROOM_CTR[1]) for _card in self.env['cards'] if _card.cardtype == 2]

    def get_destiny(self):
        return [(_card.position['x']-ENV_CON.ROOM_CTR[0], _card.position['y']-ENV_CON.ROOM_CTR[1]) for _card in self.env['cards'] if _card.cardtype == 1]

    def get_others_distance(self):
        rank = [(self.me.cal_vector(_other.get_position())[0], _other) for _other in self.others]
        rank = sorted(rank, key = lambda x: x[0])
        return list(map(lambda x: x[1], rank))

    def get_others_degree(self):
        counter = (self.me.get_rotate_dir() == -1)
        rank = [(self.me.cal_rel_degree(_other.get_position()), _other) for _other in self.others]
        rank = sorted(rank, key = lambda x: x[0])
        return list(map(lambda x: x[1], rank))

    def get_others_score(self):
        rank = [(_other.get_score(), _other) for _other in self.others]
        rank = sorted(rank, key = lambda x: x[0], reverse=True)
        return list(map(lambda x: x[1], rank))

    def cal_vector(self, pos):
        r = math.sqrt(pos[0]**2+pos[1]**2)
        theta = cal_arc(pos[0], pos[1])
        return (r, theta)

    def cal_point_clear_radius(self, pos):
        radiuses = []
        for player in self.env["player"]:
            p1 = player.char.get_center()
            rr = math.sqrt( (p1[0] - pos[0])**2 + (p1[1] - pos[1])**2) - (ENV_CON.BALL_SIZE/2)
            radiuses.append(rr)
        radiuses.sort()
        return max(radiuses[0], 0)

    def cal_way_clear_width(self, pos1, pos2):
        pass

    def check_degree_eq(self, d1, d2, eps=0.05):
        arc = d1-d2;
        while arc > 2*math.pi:
            arc -= 2*math.pi
        while arc < 0:
            arc += 2*math.pi
        return arc <= eps
