import csv
import Config.game_config as GAME_CON

class Q_learning():

    def __init__(self, a, g):
        self.Q_table = [{"000":0}, {"000":0}, {"000":0}, {"000":0}]
        #回中間, 衝A, 衝B, 衝C
        self.alpha = a
        self.gamma = g

    def get_action(self, state):
        if state not in self.Q_table[0]:
            return 0
        Qmax = max(self.Q_table[0][state], self.Q_table[1][state], self.Q_table[2][state], self.Q_table[3][state])
        if Qmax == self.Q_table[0][state]:
            return 0
        if Qmax == self.Q_table[1][state]:
            return 1
        if Qmax == self.Q_table[2][state]:
            return 2
        if Qmax == self.Q_table[3][state]:
            return 3

    """
    def write(self):
        print("begin to write dic.")
        with open('Q_table.csv', 'w') as csvfile:
            fieldnames = self.Q_table[0].keys()
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow(self.Q_table[0])
            writer.writerow(self.Q_table[1])
            writer.writerow(self.Q_table[2])
            writer.writerow(self.Q_table[3])
        print("dic written.")
    """
    
    def update(self, prev_state, prev_action, cur_state, reward):
        #print(GAME_CON.WRITE_DIC)
        #print(GAME_CON.TIMEUP)
        #print("-")
        if not GAME_CON.WRITE_DIC:
            if cur_state not in self.Q_table[0]:
                for col in self.Q_table:
                    col[cur_state] = 0
            if prev_state not in self.Q_table[0]:
                for col in self.Q_table:
                    col[prev_state] = 0
            maxQ = max(self.Q_table[0][cur_state], self.Q_table[1][cur_state], self.Q_table[2][cur_state], self.Q_table[3][cur_state])
            self.Q_table[prev_action][prev_state] = self.Q_table[prev_action][prev_state] + self.alpha * (reward + self.gamma * maxQ - self.Q_table[prev_action][prev_state])

        """
        if GAME_CON.TIMEUP:
            GAME_CON.TIMEUP = False
            GAME_CON.WRITE_DIC = True
            print("begin to write dic.")
            with open('Q_table.csv', 'w') as csvfile:
                fieldnames = self.Q_table[0].keys()
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                writer.writeheader()
                writer.writerow(self.Q_table[0])
                writer.writerow(self.Q_table[1])
                writer.writerow(self.Q_table[2])
                writer.writerow(self.Q_table[3])
            print("dic written.")
        """
