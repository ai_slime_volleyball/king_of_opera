from sys import exit

import pygame
from pygame.locals import *

import Event
from Event.base_event import BaseEvent

import Config.event_config as EVENT_CON
import Config.game_config as GAME_CON
background = pygame.image.load("screen/bg-light.jpg")
gallow = pygame.image.load("screen/hangman/gallows.png")
hanged = pygame.image.load("screen/hangman/hanged.png")
human = pygame.image.load("screen/hangman/human.png")
pace = pygame.image.load("screen/hangman/pace.png")
road = pygame.image.load("screen/hangman/road.png")

class _sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.image=m
        self.rect=self.image.get_rect()
        self.rect.topleft=(x,y)
class h_sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.image=m
        self.rect=self.image.get_rect()
        self.rect.topleft=(x,y)
        self.x=x
        self.y=y
        self.s = 5
    def walk(self,dx) :
        if self.x !=500+dx :
            self.x=500+dx 
            self.y+=self.s
            self.s=-self.s
            self.rect.topleft=(self.x,self.y)
            

road_sprite= _sprite(500,510,road)
hanged_sprite=_sprite(800,415,hanged)
gallow_sprite=_sprite(800,415,gallow)
human_sprite=h_sprite(500,450,human)#y must change
all_group = pygame.sprite.Group()
all_group.add(road_sprite)
all_group.add(gallow_sprite)
all_group.add(human_sprite)
roadlen=800-500
class EventInitTimer(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
    def do_action(self):
        self.env["uic"].add_event(EventDrawTimer(False,0,self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
class EventDrawTimer(BaseEvent):
    def __init__(self, tu, pn, env, ms):
        self.env = env
        self.priority = ms
        self.timeup = tu
        self.surf = env["screen"]
        self.time = self.env["timer"]
        self.pace_n = pn
    def do_action(self):
        if self.time<GAME_CON.TOTAL_TIME :
            now=roadlen*self.time//GAME_CON.TOTAL_TIME
            human_sprite.walk(now)
            if (now+7)//27 > self.pace_n :
                if self.pace_n % 2 == 0 :
                    py = 487
                else :
                    py = 491
                tmp=_sprite(500+self.pace_n*27,py,pace)
                self.pace_n+=1
                all_group.add(tmp)
        elif self.timeup == False :
            GAME_CON.TIMEUP = True
            GAME_CON.WRITE_DIC = True
            print("-------------------timeup-------------------")
            
            all_group.remove(human_sprite)
            all_group.remove(gallow_sprite)
            all_group.add(hanged_sprite)
            self.timeup = True
        #all_group.clear(self.surf,background)
        #all_group.draw(self.surf)
        self.env["uic"].add_event(EventDrawTimer(self.timeup,self.pace_n,self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
