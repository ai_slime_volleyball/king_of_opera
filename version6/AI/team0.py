'''
    AI template
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "Template" # 隊伍名稱
        # 這裡可以放變數暫存一些資訊

    def decide(self, helper):
        # 在這裡設計屬於你們的 AI 吧！
        return ActionUnPress
