import math
import random
from AI.base_ai import BaseAi
from AI.ai_config import *
from AI.Q_learning import Q_learning
import Config.game_config as GAME_CON

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "Q-learning"
        self.target = "center"
        self.target_pos = (0, 0)
        self.prev_state = "000"
        self.prev_action = 0
        self.prev_score = 0
        self.agent = Q_learning(0.6, 0.99)
        self.players = None
        self.count = 0

    def get_action(self, players, state):
        #1->撞A, 2->撞B, 3->撞C, 0->回中間
        self.count += 1
        #print(self.count)
        if random.uniform(0, 1) > 1 - math.e ** (self.count * -0.005):
            choice = [0]
            i = 1
            for player in players:
                choice.append(i)
                i += 1
            return random.choice(choice)  
        else:
            return self.agent.get_action(state)
        """
        if str(players) > 0:
            choice.append(1)
        if str(players) > 1:
            choice.append(2)
        if str(players) > 2:
            choice.append(3)
        """

    def update_Q_table(self, helper):
        state = helper.me.get_Qstate(helper)
        score = helper.me.get_Qscore()
        self.agent.update(self.prev_state, self.prev_action, state, score - self.prev_score)
        #self.prev_state = state
        self.prev_score = score

    def decide(self, helper):
        """
        if GAME_CON.TIMEUP:
            GAME_CON.TIMEUP = False
            self.agent.write()
            return ActionPress
        """
        
        if self.target == None:
            self.prev_state = helper.me.get_Qstate(helper)
            self.players = helper.get_others_degree()
            self.prev_action = self.get_action(helper.get_others_degree(), self.prev_state)
            
            if self.prev_action == 3 and len(self.players) == 3:
                self.target = self.players[2]
                self.target_pos = self.players[2].get_position()
            elif self.prev_action >= 2 and len(self.players) >= 2:
                self.prev_action = 2
                self.target = self.players[1]
                self.target_pos = self.players[1].get_position()
            elif self.prev_action >= 1 and len(self.players) >= 1:
                self.prev_action = 1
                self.target = self.players[0]
                self.target_pos = self.players[0].get_position()
            else:
                self.prev_action = 0
                self.target = "center"
                self.target_pos = (0, 0)

        if helper.me.check_facing(self.target_pos, 0.1):
            #player.cal_vector(pos): 計算 pos 相對於player的位置 ( 回傳(r, theta) )
            r, theta = helper.me.cal_vector(self.target_pos)
            if r < 50:
                self.update_Q_table(helper)
                self.target = None
                return ActionUnPress
            else:
                return ActionPress
        else:
            return ActionUnPress
