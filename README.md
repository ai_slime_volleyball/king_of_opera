# Challenge 2014 - Browser Wars

以下為version2(27 states)中，要改另外複製一份，不要動version2裡的檔

#修改states分法，需要改：
*helper.py中的get_Qstate
*teamtraining.py中的self.prev_state（與其它）、get_action、decide
*Q_learning.py中的get_action

#修改action分法，需要改：
*teamtraining.py中的self.prev_action（與其它）、get_action、decide
*Q_learning.py中的get_action、self.Q_table的宣告、

#修改reward，需要改：
*game_event.py中的self.player.Qscore（Line 280 & 295）

#version
*version1 (23萬 states)
[我離圓心的距離, 我speed＋我角度 or 不轉, 對手離圓心的距離, 對手speed＋我角度 or 不轉, 對手><, 我和對手相對角度, 我和對手分數相對高低]
= [5 * (8 * 2 + 1)]^2 * 2 * 8 * 2
= 231200

*version2 (27 states)
依照相對於自己的距離由近到遠排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來、沒向我衝來

*version3 (20 states)
和自己中間沒有阻礙的人(B,C,D)中，離中心最遠者
或是都在中心
再分成離自己遠，和離自己近


*version4 (125 states)
依照相對於自己的距離由近到遠排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

*version5 (27 states)
依照相對於自己延目前方向旋轉的角度由小到大排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來、沒向我衝來

*version6 (125 states)
依照相對於自己延目前方向旋轉的角度由小到大排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

*version7 (27 states)
依照分數由高到低排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來、沒向我衝來

*version8 (125 states)
依照分數由高到低排序
[player1狀態, player2狀態, player3狀態]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

*version9 (135 states)
依照相對於自己的距離由近到遠排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來、沒向我衝來
離圓心位置切成五等份

*version10 (625 states)
依照相對於自己的距離由近到遠排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

*version11 (135 states)
依照相對於自己延目前方向旋轉的角度由小到大排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來、沒向我衝來

*version12 (625 states)
依照相對於自己延目前方向旋轉的角度由小到大排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

*version13 (135 states)
依照分數由高到低排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來、沒向我衝來

*version14 (625 states)
依照分數由高到低排序
[player1狀態, player2狀態, player3狀態, 我離圓心的位置]
對手狀態包含：死亡、向我衝來且離我近、沒向我衝來且離我近、向我衝來且離我遠、沒向我衝來且離我遠

