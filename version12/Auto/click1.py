'''
    team2 ai test
'''
import pygame
from pygame.locals import *
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.env = env
        self.name = "team1"
    def decide(self, helper):
        if self.env["pyA"]:
            return AI.ai_config.ActionSkillPower
        if self.env["py1"]:
            return AI.ai_config.ActionPress
        else:
            return AI.ai_config.ActionUnPress

